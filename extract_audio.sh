for file in originals/*; do
  inputname=`basename "$file"`
  suffix="${inputname##*.}"
  name="${inputname%.*}"
  outputname="audio/samples/$name.wav"
  ffmpeg -y -i $file -vn $outputname
  sox $outputname tmp.wav silence 1 0 0.1% reverse
  sox tmp.wav $outputname silence 1 0 0.1% reverse
done

rm tmp.wav
