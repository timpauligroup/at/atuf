from itertools import cycle

from orchestra import Sample, SampleLong, SamplePool, SamplerNote
from utility import get_sample, inter_stan, print_list

from .functions import Chord, Drum, Inter

rootpitch = 44100 / 1024


# NOTES
def tro(notes, samples, reverbs, sampleon, reverbon):
    result = []
    pitch1 = 1
    for n, s in zip(notes, cycle(samples)):
        delay = n.delay
        dur = n.duration
        amp = n.amp
        pitch2 = n.freq
        rev = n.phase
        revn = get_sample(n.rel_amp, reverbs)
        release = dur * 0.01
        tmp = SamplerNote.new(delay, dur, s, amp, 0, release,
                              pitch1, pitch2, rev, revn, 0, sampleon, reverbon)
        result.append(tmp)
    return result


def part1note(note, samplepools, reverb, sampleon, revon, basson, bassrevon):
    delay = note.delay
    dur = note.duration
    amp = inter_stan(note.amp, 0.5, 1)
    rev = inter_stan(note.phase, 0.1, 1)
    dur = inter_stan(note.freq, dur / 2, dur)
    sample = get_sample(note.rel_amp, samplepools)
    basscheck = note.rel_amp < 0.33
    sineamp = amp / 2 if basscheck else 0
    sampleon = basson if basscheck else sampleon
    revon = bassrevon if basscheck else revon
    release = dur * 0.01
    return SamplerNote.new(delay, dur, sample, amp, 0, release, 1, 1, rev,
                           reverb, 0.005, sampleon, revon,
                           rootpitch, sineamp, releasecheck=False)


def part2note(pattern, samplepool, reverb, addpitches, sampleon, reverbon):
    chord = []
    dur = sum([x.duration for x in pattern]) * 2
    release = dur * 0.01
    attack = 0.9 * dur
    for i, note in enumerate(pattern):
        amp = inter_stan(note.amp, 0.5, 1)
        rev = inter_stan(note.phase, 0.9, 1)
        pitch = inter_stan(note.freq, 1, 2)
        sinefreq = rootpitch * pitch
        sinefreq = sinefreq / 2 if sinefreq > 80 else sinefreq
        sineamp = addpitches[1] if i == 0 else 0

        def thisnote(factor, d):
            return SamplerNote.new(d, dur, samplepool, amp,
                                   attack, release,
                                   factor * pitch, factor * pitch,
                                   rev, reverb,
                                   0, sampleon, reverbon, sinefreq, sineamp)
        d = 0
        for p in addpitches[0]:
            tmp = thisnote(p, d)
            chord.append(tmp)
        if i == len(pattern) - 1:
            d = dur
        tmp = thisnote(1, d)
        chord.append(tmp)
    return chord


def part1internote(note, sample, reverb, sampleon, reverbon, pitch=1):
    delay = note.delay
    dur = note.duration
    amp = inter_stan(note.freq, 0.45, 0.55)
    rev = inter_stan(note.phase, 0.1, 1)
    pitch_end = inter_stan(note.rel_amp, 0.99 * pitch, pitch)
    dur = inter_stan(note.amp, dur * 0.8, dur)
    release = dur * 0.01
    return SamplerNote.new(delay, dur, sample, amp, 0, release,
                           pitch, pitch_end, rev,
                           reverb, 0, sampleon, reverbon, releasecheck=False)


def part2internote(note, sample, reverb, sampleon, reverbon, maxdur=1):
    factor = 2
    delay = note.delay * factor
    dur = note.duration * factor
    while dur > maxdur:
        dur = dur / 2
    amp = inter_stan(note.freq, 0.45, 0.55)
    rev = inter_stan(note.phase, 0.9, 1)
    pitch = inter_stan(note.rel_amp, 0.5, 1)
    release = dur * 0.01
    return SamplerNote.new(delay, dur, sample, amp, 0, release,
                           pitch, pitch, rev,
                           reverb, 0, sampleon, reverbon, attackcheck=True)


def part3finalnote(note, samples, reverbs, sampleon, revon, basson, bassrevon):
    delay = note.delay
    dur = note.duration
    amp = 1
    rev = 1
    revn = get_sample(note.amp, reverbs)
    sample = get_sample(note.rel_amp, samples)
    basscheck = note.rel_amp < 0.25
    sineamp = amp / 2 if basscheck else 0
    sampleon = basson if basscheck else sampleon
    revon = bassrevon if basscheck else revon
    dur = inter_stan(note.phase, dur / 32, dur / 16)
    dur = 10 if 0.5 < note.rel_amp and note.rel_amp < 0.75 else dur
    result = SamplerNote.new(delay, dur, sample, amp, 0, dur * 0.99,
                             1, 1, rev, revn, 0, sampleon, revon,
                             rootpitch, sineamp)
    return result


# PARTS
def make_part1a(structure, m_samples, mrev, main_on, mrev_on,
                mainbass_on, bassrev_on,
                i_samples, irev, inter_on, irev_on):
    iiter = i_samples.__iter__()
    result = []
    for i, s in enumerate(structure):
        if type(s) is Drum:
            tmp = [part1note(x, m_samples, mrev, main_on,
                             mrev_on, mainbass_on, bassrev_on) for x in s]
        elif type(s) is Inter:
            sample = next(iiter)
            pitch = sample[1]
            sample = sample[0]
            tmp = [part1internote(x, sample, irev, inter_on, irev_on, pitch)
                   for x in s]
        else:
            raise ValueError('No Drum or Inter.')
        if i == len(structure) - 1:
            tmp = [t.change_dur(tmp[-1].delay)for t in tmp]
        result.extend(tmp)
    return result


def thebuildup():
    yield ([], 0)
    yield ([], 0.01)

    yield ([2], 0.015)
    yield ([0.5], 0.02)

    yield ([0.5, 2], 0.025)
    yield ([0.5, 2], 0.03)

    yield ([0.5, 2, 1], 0.033)
    yield ([0.5, 2, 2], 0.036)
    yield ([0.5, 2, 0.5], 0.04)

    yield ([0.5, 2, 1, 2], 0.043)
    yield ([0.5, 2, 1, 0.5], 0.046)
    yield ([0.5, 2, 0.5, 2], 0.05)

    yield ([0.5, 2, 1, 0.5, 2], 0.06)

    yield ([0.5, 2, 1, 0.5, 2, 1], 0.063)
    yield ([0.5, 2, 1, 0.5, 2, 2], 0.066)
    yield ([0.5, 2, 1, 0.5, 2, 0.5], 0.07)

    yield ([0.5, 2, 1, 0.5, 2, 1, 2], 0.073)
    yield ([0.5, 2, 1, 0.5, 2, 1, 0.5], 0.076)
    yield ([0.5, 2, 1, 0.5, 2, 0.5, 2], 0.08)

    yield ([0.5, 2, 1, 0.5, 2, 1, 0.5, 2], 0.09)

    yield ([0.5, 0.5, 0.5, 1, 1, 2, 2, 2, 1.5], 0.095)
    yield ([0.5, 0.5, 0.5, 1, 1, 2, 2, 2, 1.5, 1.5], 0.1)
    yield ([], 0)


def make_part2a(structure, m_samples, mrev, main_on, mrev_on,
                i_samples, irev, inter_on, irev_on):
    iiter = cycle(i_samples)
    buildup = thebuildup()
    result = []
    for i, s in enumerate(structure):
        if type(s) is Chord:
            addpitches = next(buildup)
            tmp = part2note(s, m_samples, mrev, addpitches, main_on, mrev_on)
        elif type(s) is Inter:
            tmp = []
            for x in s:
                sample = next(iiter)
                dur = sample[1]
                sample = sample[0]
                note = part2internote(
                    x, sample, irev, inter_on, irev_on, dur)
                tmp.append(note)
        else:
            raise ValueError('No Chord or Inter.')
        if i == len(structure) - 1:
            tmp = [t.change_dur(tmp[-1].delay)for t in tmp]
        result.extend(tmp)
    return result


def edit_part3a(part3a, split1a, introa):
    part3a[17] = split1a[-1].change_del(part3a[0].delay)
    part3a[24] = introa[-1].change_del(
        part3a[0].delay).change_dur(part3a[0].duration).change_pfield(5, 1)
    return part3a


# SAMPLES
def make_samplepools1():
    return [SamplePool('audio/drums/kicks/*.wav', SampleLong),
            SamplePool('audio/drums/hats/*.wav', SampleLong),
            SamplePool('audio/drums/snares/*.wav', SampleLong)]


def make_intersamples1(folder):
    names = [(folder + 'alien-click-bounce-1.wav', 1),  # check
             (folder + 'places-distortedrise-bounce-1.wav', 1),  # check
             (folder + 'alien-dog3-bounce-1.wav', 1),  # check
             (folder + 'piano-distorted2-bounce-1.wav', 1),  # check short
             (folder + 'coldplay-rawsession-bounce-1.wav', 1),  # check short
             (folder + 'piano-distorted1-bounce-1.wav', 1),  # check short
             (folder + 'coldplay-ruleworld-bounce-1.wav', 1),  # check short
             (folder + 'browning-boom-bounce-1.wav', 1),  # check short
             (folder + 'coldplay-wicked-bounce-1.wav', 0.9),  # check d
             (folder + 'places-ah-bounce-1.wav', 1),  # check
             (folder + 'coldplay-distortedrise-bounce-1.wav', 1),  # ??
             (folder + 'piano-fasttrack-bounce-1.wav', 1),  # exchanged!!!
             (folder + 'piano-distorted3-bounce-1.wav', 1),  # check
             (folder + 'piano-distorted4-bounce-1.wav', 1),  # check
             (folder + 'coldplay-distorteddrums-bounce-1.wav', 1),  # check
             (folder + 'places-metal-bounce-1.wav', 1),  # exchanged!!!
             (folder + 'coldplay-itwasa-bounce-1.wav', 0.8),  # check d
             (folder + 'piano-chord-bounce-1.wav', 1),  # check
             (folder + 'alien-dog2-bounce-1.wav', 1),  # check
             (folder + 'browning-texas-bounce-1.wav', 0.8)]  # check d
    print_list(names)
    samples = [(Sample(n), p) for n, p in names]
    assert(len(samples) == 20)
    return samples


def make_intersamples2(folder):
    names = [(folder + 'tacticool-gijoe-bounce-1.wav', 0.3),
             (folder + 'p90-counterstrike-bounce-1.wav', 0.2),
             (folder + 'places-distortedcry-bounce-1.wav', 10),  # exchanged!!!
             (folder + 'tacticool-stuff-bounce-1.wav', 0.6),
             (folder + 'tacticool-fullmetal-bounce-1.wav', 0.1),
             (folder + 'p90-callofduty-bounce-1.wav', 0.2),
             (folder + 'coldplay-distortedchord2-bounce-1.wav', 10),
             (folder + 'places-mmh-bounce-1.wav', 10),
             (folder + 'p90-battlefield-bounce-1.wav', 0.1),
             (folder + 'places-distortedsigh-bounce-1.wav', 10),
             (folder + 'coldplay-distortedchord1-bounce-1.wav', 10),
             (folder + 'browning-bacon-bounce-1.wav', 0.4),
             (folder + 'places-piano-bounce-1.wav', 10),
             (folder + 'places-noise-bounce-1.wav', 10)]  # exchanged!!!
    print_list(names)
    samples = [(Sample(n), d) if d >= 1 else (SampleLong(n, True), d)
               for n, d in names]
    assert(len(samples) == 14)
    return samples


def make_allsounds(part1, part2, folder):
    names = [folder + 'alien-rimshot1-bounce-1.wav',
             folder + 'alien-dog1-bounce-1.wav',
             folder + 'alien-rimshot2-bounce-1.wav',
             folder + 'coldplay-clap1-bounce-1.wav',
             folder + 'coldplay-clap2-bounce-1.wav']
    return part1 + [SamplePool('', Sample, names)] + part2
