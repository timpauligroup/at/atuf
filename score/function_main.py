from itertools import cycle

from cyscore import Note, Score, Voice

from ensemble import Clipper
from ensemble.concatenate_debug import concatenate_videoclips
from orchestra import Sample, SampleLong, SamplePool
from utility import xbarinsecs

from .functions import (connect, make_abstract, make_concrete, make_final,
                        make_gens, make_interludes, make_intro, make_split1,
                        make_split2)
from .functions_audio import (edit_part3a, make_allsounds, make_intersamples1,
                              make_intersamples2, make_part1a, make_part2a,
                              make_samplepools1, part3finalnote, tro)
from .functions_video import (make_clippers, make_clippers1, make_clippers2,
                              make_inters1, make_inters2, make_introv,
                              make_part1v, make_part2v, make_part3v,
                              make_splitv)


def main(name, audioon, videoon, mastergain,
         drumson, drumsrevon, bassdrumson, bassdrumsrevon,
         chordson, chordsrevon,
         finalon, finalrevon, finalbasson, finalbassrevon,
         special1on, special1revon, special2on, special2revon,
         splits1on, splits1revon, splits2on, splits2revon,
         splits3on, splits3revon,
         preset='ultrafast', videofx=True):
    print('atuf')
    # general
    bpm = 170  # from vordhosbn
    duration = xbarinsecs(2**7, bpm)
    lastlenadd = 10
    print('partduration:', duration)
    gens = make_gens()
    # interludes
    print('interludes')
    interludes = make_interludes(gens, duration)
    print('len:', len(interludes))
    # with the current interlude fucntion from 04.02.2018 after the
    # 14th i we are at half duration. reverse to get more i in concrete
    interludes = list(reversed(interludes))
    # splits
    print('splits')
    intro = make_intro()
    split1 = make_split1()
    split2 = make_split2(duration)
    # concrete
    print('concrete')
    concrete = make_concrete(gens, duration)
    print('len:', len(concrete))
    print('conreal')
    split_i = 20
    conreal = connect(concrete, interludes, split_i)
    print('len:', len(conreal))
    # abstract
    print('abstract')
    abstract = make_abstract(gens, duration)
    print('len:', len(abstract))
    print('absreal')
    absreal = connect(abstract, interludes, len(interludes) - split_i, split_i)
    print('len:', len(absreal))
    print('final')
    final = make_final(conreal + absreal, duration)
    print('len:', len(final))
    # AUDIO
    if audioon:
        print('audio')
        shortrev = cycle(['short1', 'short2', 'short3',
                          'short4', 'short5', 'short6'])
        long1rev = cycle(['long11', 'long12', 'long13', 'long14'])
        long2rev = cycle(['long21', 'long22', 'long23', 'long24', 'long25'])
        reverbs = [shortrev, long1rev, long2rev]

        print('intro')
        bouncefolder = 'audio/atuf_cutting/bounce/'
        introsamples = [
            Sample(bouncefolder + 'places-distortedintro-bounce-1.wav')]
        introa = tro(intro, introsamples, reverbs, splits1on, splits1revon)

        print('part1')
        samplepools1 = make_samplepools1()
        samplesinter1 = make_intersamples1(bouncefolder)
        part1a = make_part1a(conreal,
                             samplepools1, shortrev,
                             drumson, drumsrevon, bassdrumson, bassdrumsrevon,
                             samplesinter1, long1rev,
                             special1on, special1revon)

        print('split1')
        splitsmpls = [Sample(bouncefolder + 'places-wake-bounce-1.wav')]
        split1a = tro(split1, splitsmpls, reverbs, splits2on, splits2revon)

        print('part2')
        samplepool2 = SamplePool('audio/chords/*.wav', SampleLong)
        samples_inter2 = make_intersamples2(bouncefolder)
        part2a = make_part2a(absreal,
                             samplepool2, long2rev, chordson, chordsrevon,
                             samples_inter2, long1rev,
                             special2on, special2revon)

        print('split2')
        split2a = tro(split2, splitsmpls, reverbs, splits3on, splits3revon)

        print('part3')
        allsounds = make_allsounds([samplepools1[0]] + [samplepools1[2]],
                                   [samplepool2], bouncefolder)
        longrevs = [long1rev, long2rev]
        part3a = [part3finalnote(f, allsounds, longrevs,
                                 finalon, finalrevon,
                                 finalbasson, finalbassrevon)
                  for f in final]
        # because last two single chord notes are too quiet we exchange!!!
        part3a = edit_part3a(part3a, split1a, introa)

        print('merge n render')
        parts = introa + part1a + split1a + part2a + split2a + part3a
        voi = Voice('sampler', parts)
        masternote = Note(0, voi.duration() + lastlenadd, [mastergain])
        mastervoice = Voice('master', [masternote])
        sco = Score([mastervoice, voi])
        if True:
            audio = sco.render('orchestra/sampler.orc',
                               'build/' + name + '.sco',
                               'build/' + name + '.wav',
                               logname='build/' + name + '.log',
                               mutefuncs=[lambda x: True,
                                          lambda x: x.sampleon or x.reverbon])

    else:
        audio = None

    # VIDEO
    if videoon:
        print('video')
        clippers = make_clippers()

        print('intro')
        splitter = Clipper(['video/upsized/1alien.mov'], 15)
        introv = make_introv(intro, splitter)

        print('part1')
        clippers1 = make_clippers1(clippers)
        inters1v = make_inters1()
        part1v = make_part1v(absreal, clippers1, inters1v, videofx)

        print('split1')
        split1v = make_splitv(split1, splitter)

        print('part2')
        clippers2 = make_clippers2(clippers)
        inters2v = make_inters2()
        part2v = make_part2v(conreal, clippers2, inters2v, videofx)

        print('split2')
        split2v = make_splitv(split2, splitter)

        print('part3')
        allclippers = [cycle(clippers1), cycle(clippers2)]
        part3v = make_part3v(final, allclippers, lastlenadd, videofx)

        print('merge n render')
        clips = introv + part1v + split1v + part2v + split2v + part3v
        video = concatenate_videoclips(clips)

        if True:
            video.write_videofile('build/' + name + '.avi', codec='png',
                                  audio=audio, preset=preset)
