import subprocess as sp
import tempfile as tf
from glob import iglob
from os import remove
from os.path import basename, exists

import numpy as np

from orchestra.fftwave import process_file
from utility import mkdir, square


def resample(inname, outname):
    args = ['sndfile-resample', '-to', '48000', '-c', '0', inname,
            outname]
    sp.run(args)


def process(size, realfunc, folder, filename):
    outname = folder + str(size) + realfunc.__name__ + basename(filename)
    tmpname = tf.gettempdir() + '/' + basename(outname)
    if exists(tmpname):
        remove(tmpname)
    process_file(filename, tmpname, realfunc, square, size)
    resample(tmpname, outname)


# get samples
samples = 'audio/samples/'
regex = samples + '*.wav'
names = list(iglob(regex))
assert(len(names) == 9)

# generate
fcfolder = mkdir('audio/forcutting')
for name in names:
    outname = fcfolder + basename(name)
    resample(name, outname)

# inters
print('inters')
names3 = [n for n in names if 'coldplay' in n or 'piano' in n or 'places' in n]
for name in names3:
    process(4, np.abs, fcfolder, name)

print('irs')
# impulse respones
irfolder = 'audio/long1/'
for name in iglob(irfolder + '*'):
    outname = irfolder + '48000' + basename(name)
    resample(name, outname)

# drums
print('hats')
for name in names:
    process(4096, np.abs, mkdir('audio/drums/hats'), name)

print('kicks')
for name in names:
    process(1024, np.angle, mkdir('audio/drums/kicks'), name)

print('snares')
for name in names:
    process(65536, np.angle, mkdir('audio/drums/snares'), name)

# chords
print('chords')
names2 = [n for n in names if 'coldplay' not in n and 'browning' not in n]
for name in names2:
    for size in [64, 128, 256, 512, 1024]:
        process(size, np.abs, mkdir('audio/chords/'), name)
