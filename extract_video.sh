for file in originals/*; do
  inputname=`basename "$file"`
  suffix="${inputname##*.}"
  name="${inputname%.*}"
  outputname="video/raw/$name.mov"
  ffmpeg -y -i $file -an -crf 0 $outputname
done
