from moviepy.editor import VideoFileClip
from moviepy.video.fx.all import crop

rootfolder = 'video/'
infolder = rootfolder + 'raw/'
outfolder = rootfolder + 'upsized/'
whole = (0, None)


def cut_scene(src, scene, dest):
    file_ext = '.mov'
    codec = 'libx264'
    width = 1920
    height = 1080
    factor = height / width
    scale = 'scale={}x{}'.format(width, height)
    flags = 'lanczos+full_chroma_inp+bitexact'
    paras = ['-vf', scale, '-sws_flags', flags, '-crf', '0']
    video = VideoFileClip(src).subclip(scene[0], scene[1])
    assert(video.w > video.h)
    new_height = video.w * factor
    heightStart = (video.h - new_height) // 2

    video = crop(video, x1=0, y1=heightStart,
                 width=video.w, height=new_height)
    name = dest + file_ext
    video.write_videofile(name, codec=codec, fps=25, audio=False,
                          preset='veryslow', ffmpeg_params=paras)


# original speed
cut_scene(infolder + "2mg.mov", whole, outfolder + '2mg')

cut_scene(infolder + "5stove.mov", whole, outfolder + '5stove')

cut_scene(infolder + "6piano.mov", (16, None), outfolder + '6piano')

cut_scene(infolder + "8coldplay.mov", (2, None), outfolder + '8coldplay')

cut_scene(infolder + "1alien.mov", whole, outfolder + '1alien')
