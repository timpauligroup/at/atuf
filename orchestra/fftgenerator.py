from math import pi
from typing import List

import numpy as np

from .fftnote import FftNote


def fft_generate(structure: List[FftNote], unit: float) -> List[FftNote]:
    fftin = []
    for note in structure:
        repeats = int(round(note.delay / unit))
        tmp = [note.rel_amp] * repeats
        fftin.extend(tmp)

    spec = np.fft.fft(fftin)
    spec = spec[:len(spec) // 2]

    phases = np.angle(spec)
    phases = [(x % pi) / pi for x in phases]
    amps = np.abs(spec)
    amps[0] = 0
    maximum = max(amps)
    amps = [x / maximum for x in amps]
    freqs = [i * (1 / len(spec)) for i in range(len(spec))]

    result = [FftNote.new(unit, unit, amp, amp, freq, pha)
              for amp, pha, freq in zip(amps, phases, freqs)]

    return result
