from math import floor, log2, pi
from typing import Iterable, List, Tuple

import numpy as np
from rdp import rdp

from . import FftNote


def fftrhythm(wave, epsilon, threshold) -> List[Tuple]:
    assert(1 >= threshold >= 0)
    assert(1 >= epsilon >= 0)

    spec = np.fft.fft(wave)
    spec = spec[:len(spec) // 2]

    phases = np.angle(spec)
    phases = [(x % pi) / pi for x in phases]
    amps = np.abs(spec)
    maximum = max(amps)
    amps = [(i, x / maximum) for i, x in enumerate(amps)]
    peaks = rdp(amps, epsilon)

    peaks = list(filter(lambda x: x[1] > threshold, peaks))
    last = find_end(peaks)
    peaks.append((last, 0))

    result = []
    for p1, p2, in zip(peaks, peaks[1:]):
        dur = p2[0] - p1[0]
        amp = p1[1]
        rel_amp = np.interp(amp - threshold, [0, 1 - threshold], [0, 1])
        freq = p1[0]
        freq = np.interp(freq, [0, peaks[-2][0]], [0, 1])
        pha = phases[int(p1[0])]

        note = FftNote.new(dur, dur, rel_amp, amp, freq, pha)
        result.append(note)

    return result


def find_end(peaks):
    assert(len(peaks) > 0)
    if len(peaks) == 1:
        loga = 0
    else:
        loga = log2(peaks[-1][0])
    exp = floor(loga) + 1
    last = 2**exp + peaks[0][0]
    return last


def rhythms_from(wave, windowfunc, windowsize, epsilon,
                 threshold) -> Iterable[List[FftNote]]:
    for i in range(0, len(wave), windowsize):
        buf = wave[i: i + windowsize]
        buf = buf * windowfunc(len(buf))
        result = fftrhythm(buf, epsilon, threshold)
        yield result
