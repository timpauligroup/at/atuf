sr = 48000
ksmps = 1
nchnls = 2
0dbfs = 1

alwayson "tabscaler"

alwayson "short1"
connect "sampler","short1left","short1","inleft"
connect "sampler","short1right","short1","inright"
giShort1Left ftgen 0, 0, 0, 1, "audio/ir/short/balance-mastering-flipside-soundsystem-IR-48000-24bit-parallel-stereo.wav", 0, 0, 1
giShort1Right ftgen 0, 0, 0, 1, "audio/ir/short/balance-mastering-flipside-soundsystem-IR-48000-24bit-parallel-stereo.wav", 0, 0, 2
alwayson "short2"
connect "sampler","short2left","short2","inleft"
connect "sampler","short2right","short2","inright"
giShort2Left ftgen 0, 0, 0, 1, "audio/ir/short/balance-mastering-flipside-soundsystem-IR-48000-24bit-true-stereo-L.wav", 0, 0, 1
giShort2Right ftgen 0, 0, 0, 1, "audio/ir/short/balance-mastering-flipside-soundsystem-IR-48000-24bit-true-stereo-L.wav", 0, 0, 2
alwayson "short3"
connect "sampler","short3left","short3","inleft"
connect "sampler","short3right","short3","inright"
giShort3Left ftgen 0, 0, 0, 1, "audio/ir/short/balance-mastering-flipside-soundsystem-IR-48000-24bit-true-stereo-R.wav", 0, 0, 1
giShort3Right ftgen 0, 0, 0, 1, "audio/ir/short/balance-mastering-flipside-soundsystem-IR-48000-24bit-true-stereo-R.wav", 0, 0, 2
alwayson "short4"
connect "sampler","short4left","short4","inleft"
connect "sampler","short4right","short4","inright"
giShort4Left ftgen 0, 0, 0, 1, "audio/ir/short/the-hive-funktion-one-IR-48000-24bit-parallel-stereo.wav", 0, 0, 1
giShort4Right ftgen 0, 0, 0, 1, "audio/ir/short/the-hive-funktion-one-IR-48000-24bit-parallel-stereo.wav", 0, 0, 2
alwayson "short5"
connect "sampler","short5left","short5","inleft"
connect "sampler","short5right","short5","inright"
giShort5Left ftgen 0, 0, 0, 1, "audio/ir/short/the-hive-funktion-one-IR-48000-24bit-true-stereo-L.wav", 0, 0, 1
giShort5Right ftgen 0, 0, 0, 1, "audio/ir/short/the-hive-funktion-one-IR-48000-24bit-true-stereo-L.wav", 0, 0, 2
alwayson "short6"
connect "sampler","short6left","short6","inleft"
connect "sampler","short6right","short6","inright"
giShort6Left ftgen 0, 0, 0, 1, "audio/ir/short/the-hive-funktion-one-IR-48000-24bit-true-stereo-R.wav", 0, 0, 1
giShort6Right ftgen 0, 0, 0, 1, "audio/ir/short/the-hive-funktion-one-IR-48000-24bit-true-stereo-R.wav", 0, 0, 2

alwayson "long11"
connect "sampler","long11left","long11","inleft"
connect "sampler","long11right","long11","inright"
giLong11Left ftgen 0, 0, 0, 1, "audio/ir/long1/48000AutoPark.aif", 0, 0, 1
giLong11Right ftgen 0, 0, 0, 1, "audio/ir/long1/48000AutoPark.aif", 0, 0, 2
alwayson "long12"
connect "sampler","long12left","long12","inleft"
connect "sampler","long12right","long12","inright"
giLong12Left ftgen 0, 0, 0, 1, "audio/ir/long1/48000LargeChurch.aif", 0, 0, 1
giLong12Right ftgen 0, 0, 0, 1, "audio/ir/long1/48000LargeChurch.aif", 0, 0, 2
alwayson "long13"
connect "sampler","long13left","long13","inleft"
connect "sampler","long13right","long13","inright"
giLong13Left ftgen 0, 0, 0, 1, "audio/ir/long1/48000ParkingGarage.wav", 0, 0, 1
giLong13Right ftgen 0, 0, 0, 1, "audio/ir/long1/48000ParkingGarage.wav", 0, 0, 2
alwayson "long14"
connect "sampler","long14left","long14","inleft"
connect "sampler","long14right","long14","inright"
giLong14Left ftgen 0, 0, 0, 1, "audio/ir/long1/48000StNicolaesChurch.wav", 0, 0, 1
giLong14Right ftgen 0, 0, 0, 1, "audio/ir/long1/48000StNicolaesChurch.wav", 0, 0, 2

alwayson "long21"
connect "sampler","long21left","long21","inleft"
connect "sampler","long21right","long21","inright"
giLong21Left ftgen 0, 0, 0, 1, "audio/ir/long2/balance-mastering-teufelsberg-IR-01-48000-24bit.wav", 0, 0, 1
giLong21Right ftgen 0, 0, 0, 1, "audio/ir/long2/balance-mastering-teufelsberg-IR-01-48000-24bit.wav", 0, 0, 2
alwayson "long22"
connect "sampler","long22left","long22","inleft"
connect "sampler","long22right","long22","inright"
giLong22Left ftgen 0, 0, 0, 1, "audio/ir/long2/balance-mastering-teufelsberg-IR-02-48000-24bit.wav", 0, 0, 1
giLong22Right ftgen 0, 0, 0, 1, "audio/ir/long2/balance-mastering-teufelsberg-IR-02-48000-24bit.wav", 0, 0, 2
alwayson "long23"
connect "sampler","long23left","long23","inleft"
connect "sampler","long23right","long23","inright"
giLong23Left ftgen 0, 0, 0, 1, "audio/ir/long2/balance-mastering-teufelsberg-IR-03-48000-24bit.wav", 0, 0, 1
giLong23Right ftgen 0, 0, 0, 1, "audio/ir/long2/balance-mastering-teufelsberg-IR-03-48000-24bit.wav", 0, 0, 2
alwayson "long24"
connect "sampler","long24left","long24","inleft"
connect "sampler","long24right","long24","inright"
giLong24Left ftgen 0, 0, 0, 1, "audio/ir/long2/balance-mastering-teufelsberg-IR-04-48000-24bit.wav", 0, 0, 1
giLong24Right ftgen 0, 0, 0, 1, "audio/ir/long2/balance-mastering-teufelsberg-IR-04-48000-24bit.wav", 0, 0, 2
alwayson "long25"
connect "sampler","long25left","long25","inleft"
connect "sampler","long25right","long25","inright"
giLong25Left ftgen 0, 0, 0, 1, "audio/ir/long2/balance-mastering-teufelsberg-IR-05-48000-24bit.wav", 0, 0, 1
giLong25Right ftgen 0, 0, 0, 1, "audio/ir/long2/balance-mastering-teufelsberg-IR-05-48000-24bit.wav", 0, 0, 2

connect "sampler", "outleft", "master", "inleft"
connect "sampler", "outright", "master", "inright"
connect "short1", "outleft", "master", "inleft"
connect "short1", "outright", "master", "inright"
connect "short2", "outleft", "master", "inleft"
connect "short2", "outright", "master", "inright"
connect "short3", "outleft", "master", "inleft"
connect "short3", "outright", "master", "inright"
connect "short4", "outleft", "master", "inleft"
connect "short4", "outright", "master", "inright"
connect "short5", "outleft", "master", "inleft"
connect "short5", "outright", "master", "inright"
connect "short6", "outleft", "master", "inleft"
connect "short6", "outright", "master", "inright"
connect "long11", "outleft", "master", "inleft"
connect "long11", "outright", "master", "inright"
connect "long12", "outleft", "master", "inleft"
connect "long12", "outright", "master", "inright"
connect "long13", "outleft", "master", "inleft"
connect "long13", "outright", "master", "inright"
connect "long14", "outleft", "master", "inleft"
connect "long14", "outright", "master", "inright"
connect "long21", "outleft", "master", "inleft"
connect "long21", "outright", "master", "inright"
connect "long22", "outleft", "master", "inleft"
connect "long22", "outright", "master", "inright"
connect "long23", "outleft", "master", "inleft"
connect "long23", "outright", "master", "inright"
connect "long24", "outleft", "master", "inleft"
connect "long24", "outright", "master", "inright"
connect "long25", "outleft", "master", "inleft"
connect "long25", "outright", "master", "inright"

opcode tabscale, 0, ii
    iFn, iScale xin
    iLen = ftlen(iFn)
    idx = 0
    while idx < iLen do
        iVal table idx, iFn
        iVal = iVal * iScale
        tableiw iVal, idx , iFn
        idx = idx + 1
    od
endop

instr tabscaler
    // magic values explored with tests
    tabscale giShort1Left, 0.25
    tabscale giShort1Right, 0.25
    tabscale giShort2Left, 0.25
    tabscale giShort2Right, 0.25
    tabscale giShort3Left, 0.3
    tabscale giShort3Right, 0.2
    tabscale giShort4Left, 0.3
    tabscale giShort4Right, 0.2
    tabscale giShort5Left, 0.3
    tabscale giShort5Right, 0.15
    tabscale giShort6Left, 0.35
    tabscale giShort6Right, 0.45

    tabscale giLong11Left, 0.1
    tabscale giLong11Right, 0.15
    tabscale giLong12Left, 0.02
    tabscale giLong12Right, 0.03
    tabscale giLong13Left, 0.15
    tabscale giLong13Right, 0.05
    tabscale giLong14Left, 0.05
    tabscale giLong14Right, 0.05

    tabscale giLong21Left, 0.05
    tabscale giLong21Right, 0.05
    tabscale giLong22Left, 0.05
    tabscale giLong22Right, 0.05
    tabscale giLong23Left, 0.15
    tabscale giLong23Right, 0.1
    tabscale giLong24Left, 0.05
    tabscale giLong24Right, 0.05
    tabscale giLong25Left, 0.1
    tabscale giLong25Right, 0.05
endin

opcode  mydelay, a, ai
    aIn, iTime  xin
    if iTime > 0 then
        aOut delay aIn, iTime
    else
        aOut = aIn
    endif
    xout aOut
endop

opcode  mytranseg, k, iiii
    iStartDur, iDur, iEndDur, iAmp  xin
    if (iStartDur <= 0) && (iEndDur <= 0) then
        kOut = 1
    elseif iStartDur <= 0 then
        kOut transeg iAmp, iDur - iEndDur, 0,
                    iAmp, iEndDur, -2,
                    0
    elseif iEndDur <= 0 then
        kOut transeg 0, iStartDur, 2,
                    iAmp, iDur - iStartDur, 0,
                    iAmp
    else
        kOut transeg 0, iStartDur, 2,
                    iAmp, iDur - iStartDur - iEndDur, 0,
                    iAmp, iEndDur, -2,
                    0
    endif
    xout kOut
endop

opcode  mydiskin, aa, Ski
    SName, kPitch, iSkip  xin
    iNChnls filenchnls SName
    if iNChnls == 1 then
        aLeft diskin2 SName, kPitch, iSkip, 0, -1, 16, 1048576
        aRight = aLeft
    else
        aLeft, aRight diskin2 SName, kPitch, iSkip, 0, -1, 16, 1048576
    endif
    xout aLeft, aRight
endop

instr sampler
    ; parameters
    iDur = p3
    SName strget p4
    iSkip = p5
    iAmp = p6
    iAmpStartDur = p7
    iAmpEndDur = p8
    iPitchStart = p9
    iPitchEnd = p10
    iReverb = p11
    SReverbname = p12
    iHitDur = p13
    iSampleon = p14
    iReverbon = p15
    iSineFreq = p16
    iSineAmp = p17
    ; synthesis
    ; hit
    iDur = iDur - iHitDur
    kTime timeinsts
    if  kTime < iHitDur then
        aHit = iAmp
    else
        aHit = 0
    endif
    ; real
    kAmp mytranseg iAmpStartDur, iDur, iAmpEndDur, iAmp
    kPitch transeg iPitchStart, iDur, -2, iPitchEnd ; fast falling
    aLeftReal, aRightReal mydiskin SName, kPitch, iSkip
    aSine oscil3 iSineAmp, iSineFreq
    aLeftReal mydelay (aLeftReal + aSine) * kAmp, iHitDur
    aRightReal mydelay (aRightReal + aSine) * kAmp, iHitDur
    ; sum
    aLeft = aLeftReal + aHit
    aRight = aRightReal + aHit
    ; outs
    SLeftname strcat SReverbname, "left"
    SRightname strcat SReverbname, "right"
    if iReverbon == 1 then
        outleta SLeftname, aLeft * iReverb
        outleta SRightname, aRight * iReverb
    endif
    if iSampleon == 1 then
        outleta "outleft", aLeft
        outleta "outright", aRight
    endif
endin

opcode  myreverb, aa, aaii
    aLeftin, aRightin, iTable0, iTable1  xin
    iPlen = 1024
    aLeftout ftconv aLeftin, iTable0, iPlen
    aRightout ftconv aRightin, iTable1, iPlen
    xout  aLeftout, aRightout
endop

instr short1
  // input
  aLeft inleta "inleft"
  aRight inleta "inright"
  // synthesis
  aReverbLeft, aReverbRight myreverb aLeft, aRight, giShort1Left, giShort1Right
  // output
  outleta "outleft", aReverbLeft
  outleta "outright", aReverbRight
endin

instr short2
  // input
  aLeft inleta "inleft"
  aRight inleta "inright"
  // synthesis
  aReverbLeft, aReverbRight myreverb aLeft, aRight, giShort2Left, giShort2Right
  // output
  outleta "outleft", aReverbLeft
  outleta "outright", aReverbRight
endin

instr short3
  // input
  aLeft inleta "inleft"
  aRight inleta "inright"
  // synthesis
  aReverbLeft, aReverbRight myreverb aLeft, aRight, giShort3Left, giShort3Right
  // output
  outleta "outleft", aReverbLeft
  outleta "outright", aReverbRight
endin

instr short4
  // input
  aLeft inleta "inleft"
  aRight inleta "inright"
  // synthesis
  aReverbLeft, aReverbRight myreverb aLeft, aRight, giShort4Left, giShort4Right
  // output
  outleta "outleft", aReverbLeft
  outleta "outright", aReverbRight
endin

instr short5
  // input
  aLeft inleta "inleft"
  aRight inleta "inright"
  // synthesis
  aReverbLeft, aReverbRight myreverb aLeft, aRight, giShort5Left, giShort5Right
  // output
  outleta "outleft", aReverbLeft
  outleta "outright", aReverbRight
endin

instr short6
  // input
  aLeft inleta "inleft"
  aRight inleta "inright"
  // synthesis
  aReverbLeft, aReverbRight myreverb aLeft, aRight, giShort6Left, giShort6Right
  // output
  outleta "outleft", aReverbLeft
  outleta "outright", aReverbRight
endin

instr long11
  // input
  aLeft inleta "inleft"
  aRight inleta "inright"
  // synthesis
  aReverbLeft, aReverbRight myreverb aLeft, aRight, giLong11Left, giLong11Right
  // output
  outleta "outleft", aReverbLeft
  outleta "outright", aReverbRight
endin

instr long12
  // input
  aLeft inleta "inleft"
  aRight inleta "inright"
  // synthesis
  aReverbLeft, aReverbRight myreverb aLeft, aRight, giLong12Left, giLong12Right
  // output
  outleta "outleft", aReverbLeft
  outleta "outright", aReverbRight
endin

instr long13
  // input
  aLeft inleta "inleft"
  aRight inleta "inright"
  // synthesis
  aReverbLeft, aReverbRight myreverb aLeft, aRight, giLong13Left, giLong13Right
  // output
  outleta "outleft", aReverbLeft
  outleta "outright", aReverbRight
endin

instr long14
  // input
  aLeft inleta "inleft"
  aRight inleta "inright"
  // synthesis
  aReverbLeft, aReverbRight myreverb aLeft, aRight, giLong14Left, giLong14Right
  // output
  outleta "outleft", aReverbLeft
  outleta "outright", aReverbRight
endin

instr long21
  // input
  aLeft inleta "inleft"
  aRight inleta "inright"
  // synthesis
  aReverbLeft, aReverbRight myreverb aLeft, aRight, giLong21Left, giLong21Right
  // output
  outleta "outleft", aReverbLeft
  outleta "outright", aReverbRight
endin

instr long22
  // input
  aLeft inleta "inleft"
  aRight inleta "inright"
  // synthesis
  aReverbLeft, aReverbRight myreverb aLeft, aRight, giLong22Left, giLong22Right
  // output
  outleta "outleft", aReverbLeft
  outleta "outright", aReverbRight
endin

instr long23
  // input
  aLeft inleta "inleft"
  aRight inleta "inright"
  // synthesis
  aReverbLeft, aReverbRight myreverb aLeft, aRight, giLong23Left, giLong23Right
  // output
  outleta "outleft", aReverbLeft
  outleta "outright", aReverbRight
endin

instr long24
  // input
  aLeft inleta "inleft"
  aRight inleta "inright"
  // synthesis
  aReverbLeft, aReverbRight myreverb aLeft, aRight, giLong24Left, giLong24Right
  // output
  outleta "outleft", aReverbLeft
  outleta "outright", aReverbRight
endin

instr long25
  // input
  aLeft inleta "inleft"
  aRight inleta "inright"
  // synthesis
  aReverbLeft, aReverbRight myreverb aLeft, aRight, giLong25Left, giLong25Right
  // output
  outleta "outleft", aReverbLeft
  outleta "outright", aReverbRight
endin

instr master
    aLeft inleta "inleft"
    aRight inleta "inright"
    iAmp = p4
    outs aLeft * iAmp, aRight * iAmp
endin
