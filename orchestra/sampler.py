from glob import iglob
from itertools import cycle

import soundfile as sf
from cyscore import Note

from utility import print_list


class Skipper:
    def __init__(self, duration, start: float=0, loop=False):
        self.current = start
        self.wholedur = duration
        self.loop = loop

    def get(self, duration):
        if self.loop and self.current > self.wholedur:
            self.current = 0
        old = self.current
        self.current += duration
        return old


class Sample:
    def __init__(self, path: str) -> None:
        self.path = path
        info = sf.info(path)
        self.duration = info.duration

    def get(self, duration):
        return self.path, 0


class SampleLong(Sample):
    def __init__(self, path: str, loop=False) -> None:
        super().__init__(path)
        self.skipper = Skipper(self.duration, loop=loop)

    def get(self, duration):
        return self.path, self.skipper.get(duration)


class SamplePool(Sample):
    def __init__(self, sampleglob: str, sampletype, addnames=[]) -> None:
        names = list(sorted(iglob(sampleglob)))
        names += addnames
        print_list(names)
        assert(len(names) > 0)
        self.samples = cycle([sampletype(n) for n in names])

    def get(self, duration):
        start = 1
        wholedur = 0
        while start > wholedur:
            sample = next(self.samples)
            path, start = sample.get(duration)
            wholedur = sample.duration
        return path, start


class SamplerNote(Note):
    @classmethod
    def new(cls, delay: float, dur: float, sample: SamplePool,
            amp: float, amp_start_dur: float, amp_end_dur: float,
            pitch_start: float, pitch_end: float,
            reverb: float, reverbname, hit_dur: float,
            sampleon: bool=True, reverbon: bool=True,
            sinefreq: float=50, sineamp: float=0,
            releasecheck: bool=True, attackcheck: bool=False) -> 'SamplerNote':
        if releasecheck:
            if amp_end_dur < 0.05:
                amp_end_dur = 0.05
            dur += 0.05
        assert(0 <= amp <= 1)
        assert(0 <= amp_start_dur)
        assert(0 <= amp_end_dur)
        assert(amp_start_dur + amp_end_dur < dur)
        assert(0 < pitch_start)
        assert(0 < pitch_end)
        assert(0 <= reverb <= 1)
        assert(0 <= hit_dur)
        sample_name, skip = sample.get(dur)
        if attackcheck and skip > 0 and amp_start_dur < 0.05:
            amp_start_dur += 0.05
        return cls(delay, dur,
                   [sample_name, skip, amp,
                    amp_start_dur, amp_end_dur, pitch_start, pitch_end,
                    reverb, next(reverbname), hit_dur,
                    int(sampleon), int(reverbon),
                    sinefreq, sineamp])

    @property
    def sample(self) -> str:
        return self.pfields[0]

    @property
    def skip(self) -> float:
        return self.pfields[1]

    @property
    def amp(self) -> float:
        return self.pfields[2]

    @property
    def amp_start_dur(self) -> float:
        return self.pfields[3]

    @property
    def amp_end_dur(self) -> float:
        return self.pfields[4]

    @property
    def pitch_start(self) -> float:
        return self.pfields[5]

    @property
    def pitch_end(self) -> float:
        return self.pfields[6]

    @property
    def reverb(self) -> float:
        return self.pfields[7]

    @property
    def reverbname(self) -> float:
        return self.pfields[8]

    @property
    def hit_dur(self) -> float:
        return self.pfields[9]

    @property
    def sampleon(self) -> float:
        return self.pfields[10]

    @property
    def reverbon(self) -> float:
        return self.pfields[11]
