from itertools import cycle
from typing import Iterable, List

from cyscore import Note

Cells = List[Iterable[List[Note]]]


class FractalsGenerator:
    def __init__(self, cells: Cells) -> None:
        self.cells = cycle(cells)

    def generate(self, duration: float, maxdur: float) -> List[Note]:
        assert(duration > 0)
        assert(maxdur > 0)
        return self.__fractalize(Note(duration, duration, []), maxdur)

    def __generate(self, parent: Note, maxdur: float) -> List[Note]:
        if parent.duration <= maxdur:
            return [parent]
        else:
            result = self.__fractalize(parent, maxdur)
            return result

    def __fractalize(self, parent: Note, maxdur: float) -> List[Note]:
        cell = next(next(self.cells))
        result = []
        factor = parent.duration / sum(map(lambda x: x.duration, cell))
        current = map(lambda x: x.stretch(factor), cell)
        for note in current:
            tmp = self.__generate(note, maxdur)
            result.extend(tmp)
        return result
