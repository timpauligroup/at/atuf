from math import ceil

import numpy as np

from utility import arrays_to_wav, wav_to_arrays


def fftwave(wave, realfunc, windowfunc):
    result = wave * windowfunc(len(wave))
    result = np.fft.fft(result)
    result = result[:len(result) // 2]
    result = realfunc(result)
    return result


def process_file(srcname, destname, realfunc, windowfunc, windowsize):
    waves, sr = wav_to_arrays(srcname)
    result = []
    for channel in waves:
        n = ceil(len(channel) / windowsize)
        res_channel = []
        for buf in np.array_split(channel, n):
            res_buf = fftwave(buf, realfunc, windowfunc)
            res_channel.extend(res_buf)
        result.append(res_channel)
    arrays_to_wav(np.array(result), sr, destname)
