from cyscore import Note


class FftNote(Note):
    @classmethod
    def new(cls, delay: float, dur: float,
            rel_amp: float, amp: float, freq: float,
            phase: float) -> 'FftNote':
        assert(0 <= freq <= 1)
        assert(0 <= amp <= 1)
        assert(0 <= rel_amp <= 1)
        assert(0 <= phase <= 1)
        return cls(delay, dur, [rel_amp, amp, freq, phase])

    @property
    def rel_amp(self) -> str:
        return self.pfields[0]

    @property
    def amp(self) -> float:
        return self.pfields[1]

    @property
    def freq(self) -> str:
        return self.pfields[2]

    @property
    def phase(self) -> float:
        return self.pfields[3]
