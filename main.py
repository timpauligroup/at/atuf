from score.function_main import main

gain = 1

main('drums', True, False, gain,
     True, False, False, False,
     False, False,
     False, False, False, False,
     False, False, False, False,
     False, False, False, False, False, False)

main('drumsrev', True, False, gain,
     False, True, False, False,
     False, False,
     False, False, False, False,
     False, False, False, False,
     False, False, False, False, False, False)

main('bassdrums', True, False, gain,
     False, False, True, False,
     False, False,
     False, False, False, False,
     False, False, False, False,
     False, False, False, False, False, False)

main('bassdrumsrev', True, False, gain,
     False, False, False, True,
     False, False,
     False, False, False, False,
     False, False, False, False,
     False, False, False, False, False, False)

main('chords', True, False, gain,
     False, False, False, False,
     True, False,
     False, False, False, False,
     False, False, False, False,
     False, False, False, False, False, False)

main('chordsrev', True, False, gain,
     False, False, False, False,
     False, True,
     False, False, False, False,
     False, False, False, False,
     False, False, False, False, False, False)

main('final', True, False, gain,
     False, False, False, False,
     False, False,
     True, False, False, False,
     False, False, False, False,
     False, False, False, False, False, False)

main('finalrev', True, False, gain,
     False, False, False, False,
     False, False,
     False, True, False, False,
     False, False, False, False,
     False, False, False, False, False, False)

main('finalbass', True, False, gain,
     False, False, False, False,
     False, False,
     False, False, True, False,
     False, False, False, False,
     False, False, False, False, False, False)

main('finalbassrev', True, False, gain,
     False, False, False, False,
     False, False,
     False, False, False, True,
     False, False, False, False,
     False, False, False, False, False, False)

main('inters1', True, False, gain,
     False, False, False, False,
     False, False,
     False, False, False, False,
     True, False, False, False,
     False, False, False, False, False, False)

main('inters1rev', True, False, gain,
     False, False, False, False,
     False, False,
     False, False, False, False,
     False, True, False, False,
     False, False, False, False, False, False)

main('inters2', True, False, gain,
     False, False, False, False,
     False, False,
     False, False, False, False,
     False, False, True, False,
     False, False, False, False, False, False)

main('inters2rev', True, False, gain,
     False, False, False, False,
     False, False,
     False, False, False, False,
     False, False, False, True,
     False, False, False, False, False, False)

main('splits1', True, False, gain,
     False, False, False, False,
     False, False,
     False, False, False, False,
     False, False, False, False,
     True, False, False, False, False, False)

main('splits1rev', True, False, gain,
     False, False, False, False,
     False, False,
     False, False, False, False,
     False, False, False, False,
     False, True, False, False, False, False)

main('splits2', True, False, gain,
     False, False, False, False,
     False, False,
     False, False, False, False,
     False, False, False, False,
     False, False, True, False, False, False)

main('splits2rev', True, False, gain,
     False, False, False, False,
     False, False,
     False, False, False, False,
     False, False, False, False,
     False, False, False, True, False, False)

main('splits3', True, False, gain,
     False, False, False, False,
     False, False,
     False, False, False, False,
     False, False, False, False,
     False, False, False, False, True, False)

main('splits3rev', True, False, gain,
     False, False, False, False,
     False, False,
     False, False, False, False,
     False, False, False, False,
     False, False, False, False, False, True)

# mymaster = 0.5
#
# main('atuf_audio_dry', True, False, mymaster,
#      True, True, False, True, False, True, True, False,
#      True, False, True, False,
#      True, False, True, False, True, False)
#
# main('atuf_audio', True, False, mymaster,
#      True, True, True, True, True, True, True, True,
#      True, True, True, True,
#      True, True, True, True, True, True)
#
# main('atuf_video', False, True, 0,
#      True, True, True, True, True, True, True, True,
#      True, True, True, True,
#      True, True, True, True, True, True,
#      'veryslow')
