from itertools import groupby
from math import sqrt

import numpy as np

# single pixels sorted by distance
# pixelsets/grains sorted by length: how to split?
# at certain pixel: how to determine pixel
# at certain distance


# utility
def process_frame(frame, func):
    result = np.stack((func(line) for line in frame))
    return result


def distance(rgb1, rgb2):
    r = (rgb1[0] - rgb2[0])
    g = (rgb1[1] - rgb2[1])
    b = (rgb1[2] - rgb2[2])
    return r + g + b


def key_distance(rgb):
    d = distance((0, 0, 0), rgb)
    return d


def is_pixel_equal(pixel1, pixel2, tolerance):
    result = np.subtract(pixel1, pixel2)
    result = [abs(r) <= tolerance for r in result]
    return all(result)


def sorted_to_middle(sortedlist):
    if len(sortedlist) > 1:
        result = sortedlist[0::2] + \
            sortedlist[((len(sortedlist) // 2) * 2) - 1::-2]
    else:
        result = sortedlist
    return result


def get_nearest_pixel(line, refpixel):
    sort = sorted(line, key=lambda x: distance(refpixel, x))
    return sort[0]


def luminance(rgb):
    return sqrt(0.299 * (rgb[0]**2) + 0.587 * (rgb[1]**2)
                + 0.114 * (rgb[2]**2))


# sort pixelsets
def get_pixelsets_pixel(line, refpixel, tolerance):
    result = []
    pixelset = []
    line = [tuple(l) for l in line]
    line = [list(x[1]) for x in groupby(line)]
    for p in line:
        pixelset.extend(p)
        if is_pixel_equal(p[0], refpixel, tolerance):
            result.append(pixelset)
            pixelset = []
    first = result[0] if len(result) > 0 else []
    main = result[1:] if len(result) > 1 else []
    return first, main, pixelset


def get_pixelsets_edge(line, dis):
    result = []
    pixelset = []
    for p1, p2 in zip(line, line[1:]):
        pixelset.append(p1)
        if distance(p1, p2) >= dis:
            result.append(pixelset)
            pixelset = []
    pixelset.append(line[-1])
    first = result[0] if len(result) > 0 else []
    main = result[1:] if len(result) > 1 else []
    return first, main, pixelset


def sort_linesets(line, tolerance, refpixel, edges=False, findnearest=True):
    nearest = get_nearest_pixel(line, refpixel) if findnearest else refpixel
    first, pixelsets, last = get_pixelsets_pixel(line, nearest, tolerance)
    if not edges:
        pixelsets = [first] + pixelsets + [last]
        first = []
        last = []
    pixelsets = list(reversed(sorted(pixelsets, key=len)))
    pixelsets = sorted_to_middle(pixelsets)
    pixels = [i for sub in pixelsets for i in sub]
    result = first + pixels + last
    return np.array(result)


def get_sort_pixelsets(tolerance, refpixel, edges=False, findnearest=True):
    def sort_pixelsets(frame):
        frame = process_frame(frame, lambda x: sort_linesets(
            x, tolerance, refpixel, edges, findnearest))
        return frame
    return sort_pixelsets


# sort pixels
def sort_line(line):
    result = sorted(line, key=key_distance)
    return np.array(result)


def sort_pixels(frame):
    frame = process_frame(frame, sort_line)
    return frame
