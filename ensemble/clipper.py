from moviepy.editor import VideoFileClip
from .concatenate_debug import concatenate_videoclips


class Clipper:
    def __init__(self, videonames, start=0):
        self.videonames = videonames
        videos = [VideoFileClip(v) for v in videonames]
        self.video = concatenate_videoclips(videos)
        self.current = start

    def get(self, seconds):
        future = self.current + seconds
        clip = self.video.subclip(self.current, future)
        self.current = future
        return clip

    def __repr__(self):
        return str(self.videonames)
