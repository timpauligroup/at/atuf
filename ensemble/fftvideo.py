from itertools import chain

import numpy as np


def get_fftframefunc(realfunc, splitidxs, normalize=1):
    def fft_frame(frame):
        return process_frame(frame, lambda x: fft_pixelline(x, realfunc),
                             splitidxs, normalize)
    return fft_frame


def process_frame(frame, func, splitidxs, normalize=1):
    splitted = (np.split(line, splitidxs) for line in frame)
    processed = ((func(l) for l in line) for line in splitted)
    result = [list(chain.from_iterable(line)) for line in processed]
    if normalize > 0:
        result = normalize_frame(result, normalize)
    result = np.stack(result)
    return result


# fft
def normalize_frame(frame, scale=1):
    norm = float(max((max(pixel) for line in frame for pixel in line)))
    result = [[[(p / norm) * (scale * 255) for p in pixel]
               for pixel in line] for line in frame]
    return result


def fft_colorline(line, realfunc):
    fft = np.fft.fft(line)
    result = realfunc(fft)
    result[0] = result[1]
    result[-1] = result[-2]
    result = list(reversed(result[:len(result) // 2])) + \
        list(reversed(result[(len(result) // 2):]))
    return result


def fft_pixelline(line, realfunc):
    red = [p[0] for p in line]
    green = [p[1] for p in line]
    blue = [p[2] for p in line]
    redresult = fft_colorline(red, realfunc)
    greenresult = fft_colorline(green, realfunc)
    blueresult = fft_colorline(blue, realfunc)
    return [(r, g, b) for r, g, b in zip(redresult, greenresult, blueresult)]
