from errno import EEXIST
from math import floor
from os import makedirs

import numpy as np
import soundfile as sf


def square(size):
    return tuple(1 for i in range(size))


def wav_to_arrays(name):
    with sf.SoundFile(name, 'r') as f:
        sr = f._info.samplerate
        data = f.read(len(f), always_2d=True)
    data = tuple(data[:, i] for i in range(data.shape[1]))
    return data, sr


def arrays_to_wav(waves, sr, name):
    channels = len(waves)
    norm = float(max([max(w) for w in waves]))
    data = [w / norm for w in waves]
    for d in data:
        assert([-1 < w < 1 for w in d])
    data = np.stack(data, axis=-1)
    with sf.SoundFile(name, 'x', sr, channels, 'FLOAT') as f:
        f.write(data)
    return name


def mkdir(n):
    try:
        makedirs(n)
    except OSError as e:
        if e.errno != EEXIST:
            raise
    return n + '/'


def xbarinsecs(x, bpm):
    return x * (60 / bpm)


def inter_stan(value, start, end):
    return np.interp(value, [0, 1], [start, end])


def get_sample(peak, samples):
    """peak is between 0 and 1. samples are equally distributed"""
    assert(0 <= peak <= 1)
    length = len(samples)
    unit = 1 / length
    decision = floor(peak / unit)
    decision = length - 1 if decision == length else decision
    return samples[decision]


def print_list(l):
    for i in l:
        print(i)
